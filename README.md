# GDIR

A simple bash script to encrypt/decrypt directories with gcrypt

## Usage

```
gdir -e password dir_to_encrypt out_dir     # encrypt
gdir -d password encrypted_dir out_dir      # decrypt
gdir -c password encrypted_dir filepath     # cat file
gdir -l password encrypted_dir              # list files
gdir -R password encrypted_dir              # rename files
```
